package ru.perminov.UiPageObkectFramework.configurators;

public interface PageConfigurator<T> {
    default int getPriority() {
        return 10;
    }

    void configure(Object object);
}
