package ru.perminov.UiPageObkectFramework.configurators.implementations;

import lombok.SneakyThrows;
import ru.perminov.UiPageObkectFramework.annotations.Property;
import ru.perminov.UiPageObkectFramework.utils.Utils;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.Arrays;
import java.util.Map;
import java.util.Objects;

import static java.util.stream.Collectors.toMap;

public class PropertyAnnotationConfigurator {
    private final Map<String, String> properties;

    @SneakyThrows
    public PropertyAnnotationConfigurator() {
        String path = Objects
                .requireNonNull(ClassLoader
                        .getSystemClassLoader()
                        .getResource("application.properties"))
                .getPath();

        properties = new BufferedReader(new FileReader(path))
                .lines()
                .map(line -> line.trim().split("\s*=\s*"))
                .collect(toMap(splitLine -> splitLine[0], splitLine -> splitLine[1]));
    }

    @SneakyThrows
    public void configure(Object object) {
        Class<?> type = object.getClass();
        Arrays.stream(type.getDeclaredFields()).forEach(field -> {
            Property annotation = field.getAnnotation(Property.class);
            if (annotation != null) {
                String valueFromAnnotation = annotation.value();
                String valueToInject;

                if (valueFromAnnotation.isEmpty()) {
                    valueToInject = properties.get(field.getName());
                } else {
                    valueToInject = properties.get(valueFromAnnotation);
                }

                Object castedValue = Utils.parseString(valueToInject, field.getType());

                field.setAccessible(true);

                try {
                    field.set(object, castedValue);
                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                }
            }
        });
    }
}
