package ru.perminov.UiPageObkectFramework.configurators.implementations;

import ru.perminov.UiPageObkectFramework.stores.PageStore;
import ru.perminov.UiPageObkectFramework.annotations.Inject;
import ru.perminov.UiPageObkectFramework.configurators.PageConfigurator;

import java.util.Arrays;

public class InjectAnnotationConfigurator implements PageConfigurator<Inject> {
    @Override
    public void configure(Object object) {
        PageStore pageStore = PageStore.getInstance();
        Class<?> type = object.getClass();
        Class<Inject> injectAnnotation = Inject.class;

        Arrays.stream(type.getDeclaredFields())
                .filter(field -> field.getAnnotation(injectAnnotation) != null)
                .forEach(field -> {
                    try {
                        pageStore.createAndPutObject(field.getType());
                        field.setAccessible(true);
                        field.set(object, PageStore.getInstance().getPage(field.getType()));
                    } catch (IllegalAccessException e) {
                        e.printStackTrace();
                    }
                });
    }
}
