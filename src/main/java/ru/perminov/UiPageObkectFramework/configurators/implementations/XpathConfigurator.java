package ru.perminov.UiPageObkectFramework.configurators.implementations;

import com.codeborne.selenide.SelenideElement;
import ru.perminov.UiPageObkectFramework.annotations.selectors.Xpath;
import ru.perminov.UiPageObkectFramework.configurators.PageConfigurator;
import ru.perminov.UiPageObkectFramework.stores.SourceStore;

import java.lang.reflect.Field;
import java.util.Arrays;
import java.util.Map;

import static com.codeborne.selenide.Selenide.$x;
import static java.lang.String.format;

public class XpathConfigurator implements PageConfigurator<Xpath> {
    @Override
    public int getPriority(){
        return 2;
    }

    @Override
    public void configure(Object object) {
        Class<?> type = object.getClass();
        Map<String, String> properties = SourceStore.getInstance().getSourceForType(type);
        Class<Xpath> xPathAnnotation = Xpath.class;

        Arrays.stream(type.getDeclaredFields())
                .filter(field -> field.getAnnotation(xPathAnnotation) != null)
                .forEach(field -> {
                    try {
                        validateFieldType(field);
                        String fieldKey = getXpathKeyFromAnnotation(field, xPathAnnotation);
                        String xPathFromProperty = getXpathValueFromProperties(fieldKey, properties);
                        SelenideElement elementToInject = $x(xPathFromProperty);
                        field.setAccessible(true);
                        field.set(object, elementToInject);
                    } catch (IllegalAccessException e) {
                        e.printStackTrace();
                    }
                });
    }

    private void validateFieldType(Field field) {
        if (!field.getType().equals(SelenideElement.class)) {
            throw new RuntimeException("Field is not SelenideElement");
        }
    }

    private String getXpathKeyFromAnnotation(Field field, Class<Xpath> xPathAnnotation) {
        String fieldKey;
        fieldKey = field.getAnnotation(xPathAnnotation).value();
        if (fieldKey.equals("")) {
            fieldKey = field.getName();
        }
        return fieldKey;
    }

    private String getXpathValueFromProperties(String fieldKey, Map<String, String> properties) {
        String xPathFromProperty = properties.get(fieldKey);
        if (xPathFromProperty == null) {
            throw new RuntimeException(format("No property: %s", fieldKey));
        }
        return xPathFromProperty;
    }
}
