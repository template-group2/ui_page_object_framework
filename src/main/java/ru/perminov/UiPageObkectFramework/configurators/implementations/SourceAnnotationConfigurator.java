package ru.perminov.UiPageObkectFramework.configurators.implementations;

import lombok.SneakyThrows;
import ru.perminov.UiPageObkectFramework.annotations.Page;
import ru.perminov.UiPageObkectFramework.annotations.Property;
import ru.perminov.UiPageObkectFramework.annotations.SelectorsSource;
import ru.perminov.UiPageObkectFramework.configurators.PageConfigurator;
import ru.perminov.UiPageObkectFramework.stores.SourceStore;
import ru.perminov.UiPageObkectFramework.utils.Utils;

import java.io.BufferedReader;
import java.io.FileReader;
import java.lang.annotation.Annotation;
import java.util.Arrays;
import java.util.Map;
import java.util.Objects;

import static java.util.stream.Collectors.toMap;

public class SourceAnnotationConfigurator implements PageConfigurator {
    @Override
    public int getPriority(){
        return 0;
    }

    @SneakyThrows
    public void configure(Object object) {
        Class<?> type = object.getClass();
        SelectorsSource annotation = type.getAnnotation(SelectorsSource.class);
        if (annotation != null) {
            String path = annotation.value();
            SourceStore.getInstance().putSource(type, getPropertiesFromSource(path));
        }
    }

    @SneakyThrows
    private Map<String, String> getPropertiesFromSource(String sourcePath) {
        String path = Objects
                .requireNonNull(ClassLoader
                        .getSystemClassLoader()
                        .getResource(sourcePath))
                .getPath();

        return new BufferedReader(new FileReader(path))
                .lines()
                .map(line -> line.trim().split(" = "))
                .collect(toMap(splitLine -> splitLine[0], splitLine -> splitLine[1]));
    }
}
