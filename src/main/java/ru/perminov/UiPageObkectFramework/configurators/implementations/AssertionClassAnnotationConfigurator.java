package ru.perminov.UiPageObkectFramework.configurators.implementations;

import lombok.SneakyThrows;
import ru.perminov.UiPageObkectFramework.annotations.assertions.AssertionsClass;
import ru.perminov.UiPageObkectFramework.configurators.PageConfigurator;
import ru.perminov.UiPageObkectFramework.stores.AssertionsStore;
import ru.perminov.UiPageObkectFramework.utils.Utils;

public class AssertionClassAnnotationConfigurator   implements PageConfigurator<AssertionsClass> {

    @SneakyThrows
    public void configure(Object object) {
        Class<?> type = object.getClass();
        AssertionsClass annotation = type.getAnnotation(AssertionsClass.class);
        if (annotation != null) {
            AssertionsStore
                    .getInstance()
                    .createAndPutAssertion(
                            type,
                            Utils.createInstanceByClass(Class.forName(annotation.value()))
                    );
        }
    }
}
