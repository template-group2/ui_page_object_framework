package ru.perminov.UiPageObkectFramework;

import ru.perminov.UiPageObkectFramework.configurators.PageConfigurator;
import ru.perminov.UiPageObkectFramework.utils.Utils;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class PageCreator {
    private static PageCreator instance;
    private List<PageConfigurator> configurators = new ArrayList<>();

    public static PageCreator getInstance() {
        if (instance == null) {
            instance = new PageCreator();
        }
        return instance;
    }

    public <T> T createAndConfigureObject(Class<? extends T> type) {
        T object = Utils.createInstanceByClass(type);
        configure(object);
        return object;
    }

    public void configure(Object object) {
        try {
            configurators.forEach(configurator -> configurator.configure(object));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void createAndPutConfigurator(Class<? extends PageConfigurator> type) {
        configurators.add(Utils.createInstanceByClass(type));
        configurators = configurators
                .stream()
                .sorted(Comparator.comparingInt(PageConfigurator::getPriority))
                .collect(Collectors.toList());
    }
}
