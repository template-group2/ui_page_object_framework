package ru.perminov.UiPageObkectFramework.utils;

import lombok.SneakyThrows;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import static java.util.stream.Collectors.toMap;

public class Utils {
    public static Object parseString(String value, Class<?> type) {
        switch (type.getSimpleName()) {
            case "int":
            case "Integer":
                return Integer.parseInt(value);
            case "Boolean":
            case "boolean":
                return Boolean.parseBoolean(value);
            case "Long":
            case "long":
                return Long.parseLong(value);
            case "Double":
            case "double":
                return Double.parseDouble(value);
            case "Float":
            case "float":
                return Double.parseDouble(value);
            default:
                return value;
        }
    }

    public static List<Class<?>> getSubClasses(Class<?> type) {
        Class<?> currentClass = type;
        List<Class<?>> classes = new ArrayList<>();
        while (currentClass.getSuperclass() != null) {
            currentClass = currentClass.getSuperclass();
            classes.add(currentClass);
        }
        return classes;
    }

    @SneakyThrows
    public static <T> T createInstanceByClass(Class<T> type) {
        return type.getDeclaredConstructor().newInstance();
    }
}
