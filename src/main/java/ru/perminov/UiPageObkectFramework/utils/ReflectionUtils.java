package ru.perminov.UiPageObkectFramework.utils;

import lombok.Getter;
import lombok.extern.java.Log;
import org.reflections.Reflections;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Set;
import java.util.logging.Level;

@Log
public class ReflectionUtils {
    private ReflectionUtils() {
        throw new UnsupportedOperationException();
    }

    public static <T> List<Field> getAllFields(Class<T> tClass) {
        List<Field> allFields = new ArrayList<>();
        Class<?> current = tClass;
        do {
            allFields.addAll(Arrays.asList(current.getDeclaredFields()));
            current = current.getSuperclass();

        } while (current.getSuperclass() != null);

        return allFields;
    }

    public static <T> T newInstanceViaNoArgsConstructor(Class<T> tClass) {
        try {
            return tryMakeNewInstanceViaNoArgsConstructor(tClass);
        } catch (NoSuchMethodException e) {
            throw new RuntimeException("Не найден no args constructor в классе " + tClass.getName(), e);
        } catch (InstantiationException | InvocationTargetException | IllegalAccessException e) {
            throw new RuntimeException("Произошла ошибка при создании экземпляра класса " + tClass.getName(), e);
        }
    }

    private static <T> T tryMakeNewInstanceViaNoArgsConstructor(Class<T> tClass)
            throws NoSuchMethodException, InvocationTargetException, InstantiationException, IllegalAccessException {
        Constructor<T> constructor = tClass.getConstructor();
        constructor.setAccessible(true);
        return constructor.newInstance();
    }
}
