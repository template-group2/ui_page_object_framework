package ru.perminov.UiPageObkectFramework.utils;

import com.codeborne.selenide.Condition;
import com.codeborne.selenide.SelenideElement;
import com.codeborne.selenide.ex.ElementNotFound;
import io.qameta.allure.Step;
import lombok.Getter;
import org.assertj.core.api.SoftAssertions;
import ru.perminov.UiPageObkectFramework.assertions.AssertablePage;
import ru.perminov.UiPageObkectFramework.stores.AssertionsStore;

import java.util.ArrayList;
import java.util.List;

import static java.lang.String.format;

public class AssertionsUtils {
    @Getter
    private static final List<SoftAssertions> softAssertionsList = new ArrayList<>();

    public static SoftAssertions getSoftAssertions() {
        return softAssertionsList.get(softAssertionsList.size() - 1);
    }

    public static void resetAssertions() {
        softAssertionsList.clear();
    }

    public static SoftAssertions createSofty() {
        return new SoftAssertions();
    }

    public static <T> T assertThat(AssertablePage<T> page) {
        softAssertionsList.add(createSofty());
        return AssertionsStore
                .getInstance()
                .getConfiguredSourceForType(page);
    }

    @Step("Checking if element has value '{text}'")
    public static void hasValue(SelenideElement element, String text) {
        var value = element.getValue();
        getSoftAssertions()
                .assertThat(value)
                .as(format("Element's value is not equal to: '%s'", text))
                .isEqualTo(text);
    }

    @Step("Checking if element has '{text}'")
    public static void hasText(SelenideElement element, String text) {
        var value = element.getText();
        getSoftAssertions()
                .assertThat(value)
                .as(format("Element's text is not equal to: '%s'", text))
                .isEqualTo(text);
    }

    @Step("Checking if element contains '{text}'")
    public static void containsText(SelenideElement element, String text) {
        var value = element.getText();
        getSoftAssertions()
                .assertThat(value)
                .as(format("Element has no text: '%s'", text))
                .contains(text);
    }

    @Step("Checking element visibility")
    public static void isVisible(SelenideElement element) {
        boolean isVisible = false;
        try {
            element.shouldBe(Condition.visible);
            isVisible = true;
        } catch (ElementNotFound err) {
            System.out.println(err.getMessage());
        }
        getSoftAssertions()
                .assertThat(isVisible)
                .as("Element is missing" + element)
                .isTrue();
    }
}
