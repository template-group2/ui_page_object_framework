package ru.perminov.UiPageObkectFramework.utils;

import com.codeborne.selenide.Condition;
import com.codeborne.selenide.Selenide;
import com.codeborne.selenide.SelenideElement;

import com.codeborne.selenide.conditions.Visible;
import com.codeborne.selenide.ex.ElementNotFound;
import com.codeborne.selenide.ex.UIAssertionError;
import io.qameta.allure.Step;
import lombok.extern.java.Log;
import org.junit.jupiter.api.Assertions;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.opentest4j.AssertionFailedError;
import ru.perminov.UiPageObkectFramework.PageInitializer;

import java.time.Duration;
import java.util.Arrays;
import java.util.function.Supplier;
import java.util.logging.Level;

import static com.codeborne.selenide.Condition.*;
import static java.lang.String.format;
import static ru.perminov.UiPageObkectFramework.PageInitializer.getPackageName;

@Log
public class SelenideUtils {
    private static final long DEFAULT_WAITING_TIMEOUT = 10000;
    protected static final String CENTER = "{behavior: \"instant\", block: \"center\", inline: \"center\"}";

    @Step("Click element")
    public static void click(SelenideElement element) {
        scrollToElement(element).click();
    }

    @Step("Set value {value}")
    public static void setValue(SelenideElement element, String value) {
        scrollToElement(element).shouldBe(editable).setValue(value);
        waitEventToHappen(() -> element.getValue().equals(value));
    }

    public static SelenideElement waitElementToDisappear(SelenideElement element) {
        return element.shouldBe(hidden, Duration.ofMillis(DEFAULT_WAITING_TIMEOUT));
    }

    public static SelenideElement waitElementToAppear(SelenideElement element) {
        return element.shouldBe(visible, Duration.ofMillis(DEFAULT_WAITING_TIMEOUT));
    }

    public static SelenideElement scrollToElement(SelenideElement element) {
        return getElement(element).shouldBe(visible, Duration.ofMillis(DEFAULT_WAITING_TIMEOUT)).scrollIntoView(CENTER);
    }

    public static void waitEventToHappen(Duration duration, final Supplier<Boolean> action, String message) {
        try {
            new WebDriverWait(Selenide.webdriver().driver().getWebDriver(), duration)
                    .withMessage(message)
                    .until(driver -> action.get());
        } catch (ElementNotFound ex) {
            throw ex;
        } catch (TimeoutException ex) {
            Assertions.fail(ex.getMessage());
        }
    }

    public static void waitEventToHappen(final Supplier<Boolean> action) {
        waitEventToHappen(Duration.ofMillis(DEFAULT_WAITING_TIMEOUT), action, "WaitEventToHappen failed");
    }

    public static SelenideElement getElement(SelenideElement element) {
        try {
            element.shouldBe(visible);
            return element;
        } catch (ElementNotFound e) {
            String fileName = Arrays.stream(e.getStackTrace())
                    .filter(traceItem -> traceItem
                            .getClassName()
                            .contains(getPackageName()))
                    .findFirst()
                    .get()
                    .getFileName();
            String description = format("%nProblem is in file: '%s'%nElement:  %s", fileName, element);
            log.log(Level.WARNING, description);
            throw new AssertionFailedError(e.getMessage() +  description);
        }
    }
}
