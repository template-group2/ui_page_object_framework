package ru.perminov.UiPageObkectFramework.stores;

import ru.perminov.UiPageObkectFramework.PageCreator;

import java.util.HashMap;
import java.util.Map;

public class PageStore {
    private static PageStore instance;
    private static final Map<Class<?>, Object> store = new HashMap<>();

    public static PageStore getInstance() {
        if (instance == null) {
            instance = new PageStore();
        }
        return instance;
    }

    public void createAndPutObject(Class<?> type) {
        if (!store.containsKey(type)) {
            Object object = PageCreator.getInstance().createAndConfigureObject(type);
            store.put(type, object);
        }
    }

    public <T> T getPage(Class<T> type) {
        return (T) store.get(type);
    }
}
