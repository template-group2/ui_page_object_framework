package ru.perminov.UiPageObkectFramework.stores;

import org.assertj.core.api.SoftAssertions;
import ru.perminov.UiPageObkectFramework.PageCreator;
import ru.perminov.UiPageObkectFramework.assertions.AssertablePage;
import ru.perminov.UiPageObkectFramework.assertions.AssertionSoftyPage;
import ru.perminov.UiPageObkectFramework.configurators.implementations.InjectAnnotationConfigurator;

import java.util.HashMap;
import java.util.Map;

public class AssertionsStore {
    private static AssertionsStore instance;
    private static final Map<Class<?>, AssertionSoftyPage> assertionsStore = new HashMap<>();

    public static AssertionsStore getInstance() {
        if (instance == null) {
            instance = new AssertionsStore();
        }
        return instance;
    }

    public void createAndPutAssertion(Class<?> pageType, Object assertions) {
        if (!assertionsStore.containsKey(pageType)) {
            assertionsStore.put(pageType, new AssertionSoftyPage(assertions, new SoftAssertions()));
        }
    }

    public <T> SoftAssertions getAssertions(AssertablePage<T> type){
        return assertionsStore.get(type.getClass()).getSoftAssertions();
    }

    public <T> T getConfiguredSourceForType(AssertablePage<T> type) {
        T assertions = (T) assertionsStore.get(type.getClass()).getAssertions();
        new InjectAnnotationConfigurator().configure(assertions);
        return assertions;
    }
}
