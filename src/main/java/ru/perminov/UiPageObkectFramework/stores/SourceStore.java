package ru.perminov.UiPageObkectFramework.stores;

import lombok.Getter;
import ru.perminov.UiPageObkectFramework.PageCreator;

import java.util.HashMap;
import java.util.Map;

public class SourceStore {
    private static SourceStore instance;
    private static final Map<Class<?>, Map<String, String>> sourceStore = new HashMap<>();

    public static SourceStore getInstance() {
        if (instance == null) {
            instance = new SourceStore();
        }
        return instance;
    }

    public void putSource(Class<?> type, Map<String, String> properties) {
        if (!sourceStore.containsKey(type)) {
            sourceStore.put(type, properties);
        }
    }

    public Map<String, String> getSourceForType(Class<?> type) {
        return sourceStore.get(type);
    }
}
