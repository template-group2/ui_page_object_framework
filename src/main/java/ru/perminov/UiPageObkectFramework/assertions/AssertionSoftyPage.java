package ru.perminov.UiPageObkectFramework.assertions;

import lombok.AllArgsConstructor;
import lombok.Getter;
import org.assertj.core.api.SoftAssertions;

@AllArgsConstructor
@Getter
public class AssertionSoftyPage {
    private final Object assertions;
    private final SoftAssertions softAssertions;
}
