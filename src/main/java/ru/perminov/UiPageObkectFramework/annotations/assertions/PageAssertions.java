package ru.perminov.UiPageObkectFramework.annotations.assertions;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

@Retention(RetentionPolicy.RUNTIME)
public @interface PageAssertions {
    String value();
}
