package ru.perminov.UiPageObkectFramework.annotations.selectors;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

@Retention(RetentionPolicy.RUNTIME)
public @interface Xpath {
    String value() default "";
}
