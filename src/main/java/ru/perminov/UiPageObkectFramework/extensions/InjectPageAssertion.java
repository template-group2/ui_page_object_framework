package ru.perminov.UiPageObkectFramework.extensions;

import org.junit.jupiter.api.extension.BeforeTestExecutionCallback;
import org.junit.jupiter.api.extension.ExtensionContext;
import ru.perminov.UiPageObkectFramework.annotations.Inject;
import ru.perminov.UiPageObkectFramework.stores.PageStore;
import ru.perminov.UiPageObkectFramework.utils.ReflectionUtils;

import java.lang.reflect.Field;
import java.util.Collection;
import java.util.stream.Collectors;

public class InjectPageAssertion implements BeforeTestExecutionCallback {
    private static final String TEST_INSTANCE_ERROR = "An error occurred while getting testInstance from test execution context";
    private Collection<Field> assertionsAccessibleFields;
    private Object testInstance;

    @Override
    public void beforeTestExecution(ExtensionContext extensionContext) throws Exception {
        testInstance = extractTestInstance(extensionContext);
        extractAccessibleAssertionsFields();
        initializeAssertionsFields();
    }

    private Object extractTestInstance(ExtensionContext extensionContext) {
        return extensionContext.getTestInstance()
                .orElseThrow(() -> new RuntimeException(TEST_INSTANCE_ERROR));
    }

    private void extractAccessibleAssertionsFields() {
        assertionsAccessibleFields = ReflectionUtils.getAllFields(testInstance.getClass())
                .stream().filter(field -> field.isAnnotationPresent(Inject.class))
                .peek(field -> field.setAccessible(true))
                .collect(Collectors.toList());
    }

    private void initializeAssertionsFields() throws Exception {
        for (Field field : assertionsAccessibleFields) {
            field.setAccessible(true);
            field.set(testInstance, PageStore.getInstance().getPage(field.getType()));
        }
    }
}
