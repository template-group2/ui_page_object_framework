package ru.perminov.UiPageObkectFramework.extensions;

import org.assertj.core.api.SoftAssertions;
import org.assertj.core.api.SoftAssertionsProvider;
import org.junit.jupiter.api.extension.AfterTestExecutionCallback;
import org.junit.jupiter.api.extension.ExtensionContext;
import ru.perminov.UiPageObkectFramework.utils.AssertionsUtils;

import java.util.List;

import static ru.perminov.UiPageObkectFramework.utils.AssertionsUtils.resetAssertions;

public class AssertionExtension implements AfterTestExecutionCallback {

    @Override
    public void afterTestExecution(ExtensionContext extensionContext) {
        List<SoftAssertions> softies = AssertionsUtils.getSoftAssertionsList();
        if (softies.isEmpty()) {
            System.out.println("No assertions");
        } else {
            try{
                softies.forEach(SoftAssertionsProvider::assertAll);
            } finally {
                resetAssertions();
            }
        }
    }

}
