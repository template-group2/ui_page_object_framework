package ru.perminov.UiPageObkectFramework;

import lombok.Getter;
import org.reflections.Reflections;
import org.reflections.scanners.FieldAnnotationsScanner;
import org.reflections.scanners.SubTypesScanner;
import org.reflections.scanners.TypeAnnotationsScanner;
import ru.perminov.UiPageObkectFramework.annotations.Page;
import ru.perminov.UiPageObkectFramework.annotations.Section;
import ru.perminov.UiPageObkectFramework.annotations.assertions.PageAssertions;
import ru.perminov.UiPageObkectFramework.configurators.PageConfigurator;
import ru.perminov.UiPageObkectFramework.stores.AssertionsStore;
import ru.perminov.UiPageObkectFramework.stores.PageStore;

import java.util.Collection;
import java.util.stream.Stream;


public class PageInitializer {
    @Getter
    private final Reflections scanner;
    private static String packageName;
    private final Reflections innerScanner;

    public PageInitializer(String packageName) {
        PageInitializer.packageName = packageName;
        this.scanner = new Reflections(
                packageName,
                new FieldAnnotationsScanner(),
                new TypeAnnotationsScanner(),
                new SubTypesScanner());
        this.innerScanner = new Reflections("ru.perminov");
    }

    private static PageCreator instance;

    public static String getPackageName() {
        return packageName;
    }

    public void initPages() {
        initConfigurators();
        Stream.of(
                scanner.getTypesAnnotatedWith(Page.class),
                scanner.getTypesAnnotatedWith(Section.class)
        ).flatMap(Collection::stream)
                .forEach(type -> PageStore.getInstance().createAndPutObject(type));
    }

//    public void initAssertions(){
//        scanner.getTypesAnnotatedWith(PageAssertions.class)
//                .forEach(type -> AssertionsStore.getInstance().createAndPutAssertion(type));
//    }

    private void initConfigurators() {
        innerScanner.getSubTypesOf(PageConfigurator.class)
                .forEach(type -> PageCreator.getInstance().createAndPutConfigurator(type));
    }
}
